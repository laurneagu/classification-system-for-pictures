Laur Neagu
Universitatea Politehnica Bucuresti - Facultatea de A&C


Date generale:

Pentru rulare se merge in folderul executable si se porneste fisierul AutomationTool.exe sau 
eventual in visual studio in proiectul automationtool din folderul automationtool se merge in
folderul bin/debug si se lanseaza acelasi executabil.

Informatii generale:

exposure time raport mai mare decat  0.016 sec - indoor, mai mici outdoor.
iso value mare - indoor, mici - outdoor.
Lucrare de licenta: http://www.vision.ee.ethz.ch/teaching/sada/sadalink/reports/2011/393/report.pdf
Am calculat formula din linkul de mai jos, am aflat exposure value in functie de fnumber si exp
time si asa am recunoscut zona in care a fost facuta poza. Am folosit graficul cu valorile
din linkul urmator: http://en.wikipedia.org/wiki/Exposure_value .

Recunoasterea persoanelor dintr-un calup mare de poze se face, desi apare fereastra not
responding, aceasta NU SE VA inchide ci se va astepta pana se vor calcula toate pozele.
Dupa apasarea butonului "recognize faces" se va astepta pana la calcularea numarului de 
persoane din fiecare poza.
Am folosit Emgu CV pentru recunoastere. 
Am gandit ca default sa nu se faca recunoastere pentru ca ar dura prea mult si poate userul
nu este neaparat interesat sa sorteze pozele dupa nr de persoane.

Valide sunt considerate doar fisierele .jpg, daca sunt gasite fisiere cu alta extensie  in cale
ele sunt ignorate, la fel si cu directoarele din calea data.

Calitate:
ce e <640X480 low quality
ce e 1280X960 || 1600X1200 medium quality 
ce e >2048X1546 high quality

La pornirea recunoasterii faciale se calculeaza pentru fiecare poza in parte numarul de persoane
ce apar, pentru fiecare dintre aceastea se salveaza un model care este salvat in fisierul
trained faces iar cu toate modelele odata salvate se pot face comparari intre ele pentru a
verifica daca un utilizator apare in mai multe poze cu algoritmul Eigen Object Recognizer.

Optimizari: pentru a nu avea multa memorie ocupata utilizatorul de fiecare data cand va confirma
	iesirea din program (butonul X de pe main form si apasa pe butonul yes) se vor sterge
	automat toate persoanele recunoscute (fisiere .bmp din folderul TrainedFaces).

Potentialul aplicatiei este mare, dat fiind ca o astfel de impartire a pozelor e extrem de
atractiva pentru utilizatori, avand in vedere ca timpul a fost destul de scurt (+presesiune,
+colocvii,++) nu am apucat sa fac chiar tot ce mi-am propus, vreau sa gandesc cumva o 
redistribuire in foldere sau intr-o ordine bine gandita a pozelor dupa criterii: spre exemplu
utilizatorul selecteaza ordonarea dupa calitatea pozelor, automat el sa le poata vedea + gasi
ulterior in functie de acest criteriu ordonate. In momentul asta ii fac doar o afisare in
listviewul meu, dar as vrea sa extind pe parcurs functionalitatea aplicatiei. 
Fac sortari automate dupa criteriile cerute, aplicatia poate fi extinsa si ma gandesc sa o
dezvolt cat de curand pe Windows 8, poate si Windows Phone pentru pozele stocate in camera 
roll, eventual folosind xamarin portarea pe android si ios. Mai am insa de lucru la 
finalizarea ei si adusa intr-o forma big marketable. :D
 
Mentionez ca nu am avut suficient timp sa fac si stocarea in baza de date, si butonul de 
view models ar trebui sa ma conduca pe modelele(fetele) pozei curente, la fel nu am mai avut 
timp sa fac compararea cu celelate modele salvate. In acest moment butonul returneaza PRIMUL
model salvat.

Cu toate acestea un algoritm de similaritati intre modele exista si el se gaseste in clasa 
Recognition jos de tot metoda se numeste similarities. Rolul ei e sa compare modelele de pe 
poza curenta cu celelalte modele si sa anunte unde se mai regasesc modelele actuale.
Am testat acest algoritm pe un sample separat si functioneaza satisfacator, din pacate nu
am apucat sa il integrez aici pe moment.


