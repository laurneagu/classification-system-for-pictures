﻿using System;
using System.Drawing;
using System.Windows.Forms;
namespace ExifLibrary
{
    partial class StartForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        private ListViewColumnSorter2 lvwColumnSorter;


        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        /// 

    
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StartForm));
            this.pictures = new System.Windows.Forms.ListView();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.recognizer = new System.Windows.Forms.Button();
            this.setPath = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictures
            // 
            this.pictures.Activation = System.Windows.Forms.ItemActivation.OneClick;
            this.pictures.BackColor = System.Drawing.Color.AliceBlue;
            this.pictures.HotTracking = true;
            this.pictures.HoverSelection = true;
            this.pictures.Location = new System.Drawing.Point(12, 29);
            this.pictures.Name = "pictures";
            this.pictures.Size = new System.Drawing.Size(1165, 567);
            this.pictures.TabIndex = 0;
            this.pictures.UseCompatibleStateImageBehavior = false;
            this.pictures.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.pictures_ColumnClick);
            this.pictures.DoubleClick += new System.EventHandler(this.pictures_DoubleClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(620, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(175, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "* click on column name to sort them";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(95, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(195, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "* double click path to view photo details";
            // 
            // recognizer
            // 
            this.recognizer.Location = new System.Drawing.Point(929, 3);
            this.recognizer.Name = "recognizer";
            this.recognizer.Size = new System.Drawing.Size(164, 23);
            this.recognizer.TabIndex = 3;
            this.recognizer.Text = "recognize faces";
            this.recognizer.UseVisualStyleBackColor = true;
            this.recognizer.Click += new System.EventHandler(this.recognizer_Click);
            // 
            // setPath
            // 
            this.setPath.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.setPath.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.setPath.ForeColor = System.Drawing.Color.White;
            this.setPath.Location = new System.Drawing.Point(484, 238);
            this.setPath.Name = "setPath";
            this.setPath.Size = new System.Drawing.Size(253, 86);
            this.setPath.TabIndex = 4;
            this.setPath.Text = "set pictures directory path";
            this.setPath.UseVisualStyleBackColor = true;
            this.setPath.Click += new System.EventHandler(this.setPath_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(580, 180);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(51, 52);
            this.pictureBox1.TabIndex = 5;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Visible = false;
            // 
            // StartForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.ClientSize = new System.Drawing.Size(1200, 600);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.setPath);
            this.Controls.Add(this.recognizer);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictures);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "StartForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Classification System for Pictures - 4psa";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ListView pictures;
        private Label label1;
        private Label label2;
        private Button recognizer;
        private Button setPath;
        private PictureBox pictureBox1;
    }
}