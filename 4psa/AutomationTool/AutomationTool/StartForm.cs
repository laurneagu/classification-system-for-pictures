﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace ExifLibrary
{
    public partial class StartForm : Form
    {
        private string generalpath = "";

        public static bool withRecogn = false;

        public StartForm()
        {
            InitializeComponent();


            //pictures.BackColor = Color.GreenYellow;
            pictures.View = View.Details;
            pictures.View = System.Windows.Forms.View.Details;

            System.Windows.Forms.ColumnHeader columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            System.Windows.Forms.ColumnHeader columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            System.Windows.Forms.ColumnHeader columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            System.Windows.Forms.ColumnHeader columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            System.Windows.Forms.ColumnHeader columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            System.Windows.Forms.ColumnHeader columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            System.Windows.Forms.ColumnHeader columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));


            columnHeader1.Text = "Picture URL";
            columnHeader1.Width = 350;

            columnHeader2.Text = "Date added";
            columnHeader2.Width = 140;

            columnHeader3.Text = "Time of day";
            columnHeader3.Width = 110;

            columnHeader4.Text = "Indoor/Outdoor";
            columnHeader4.Width = 280;

            columnHeader5.Text = "Latitude";
            columnHeader5.Width = 80;

            columnHeader6.Text = "Longitude";
            columnHeader6.Width = 80;

            columnHeader7.Text = "Quality";
            columnHeader7.Width = 80;

            recognizer.Visible = false;

            // Loop through and size each column header to fit the column header text.
            foreach (ColumnHeader ch in this.pictures.Columns)
            {
                ch.Width = -2;
            }


            pictures.Activation = System.Windows.Forms.ItemActivation.OneClick;
            pictures.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {columnHeader1, columnHeader2, 
                            columnHeader3,columnHeader4, columnHeader5, columnHeader6, columnHeader7});


            // Create an instance of a ListView column sorter and assign it 
            // to the ListView control.
            lvwColumnSorter = new ListViewColumnSorter2();
            this.pictures.ListViewItemSorter = lvwColumnSorter;

        }

        private void pictures_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            // Determine if clicked column is already the column that is being sorted.
            if (e.Column == lvwColumnSorter.SortColumn)
            {
                // Reverse the current sort direction for this column.
                if (lvwColumnSorter.Order == SortOrder.Ascending)
                {
                    lvwColumnSorter.Order = SortOrder.Descending;
                }
                else
                {
                    lvwColumnSorter.Order = SortOrder.Ascending;
                }
            }
            else
            {
                // Set the column number that is to be sorted; default to ascending.
                lvwColumnSorter.SortColumn = e.Column;
                lvwColumnSorter.Order = SortOrder.Ascending;
            }

            // Perform the sort with these new sort options.
            this.pictures.Sort();
        }

        private void pictures_DoubleClick(object sender, EventArgs e)
        {
            string url = pictures.SelectedItems[0].ToString();
            string[] stringSeparators = new string[] { "{" };
            string[] result = url.Split(stringSeparators, StringSplitOptions.None);

            stringSeparators = new string[] { "}" };
            url = result[1];                 // second
            result = url.Split(stringSeparators, StringSplitOptions.None);

            url = result[result.Length - 2]; // before the last

            Variables.picturePath = url;    //set path for picture details
            Variables.pictureIndex = pictures.SelectedIndices[0];

            new Details().ShowDialog();

        }

        private void recognizer_Click(object sender, EventArgs e)
        {
            //pictures.Clear();

            System.Windows.Forms.ColumnHeader columnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            columnHeader.Text = "Faces";
            columnHeader.Width = 80;
            pictures.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] { columnHeader });

            this.recognizer.Enabled = false;
            this.recognizer.Visible = false;
            //this.pictureBox1.Visible = true;
            //this.pictures.Enabled = false; <-- reported some bugs

            // start recognition
            refillTable();
        }

        private void setPath_Click(object sender, EventArgs e)
        {
            string folderPath = "";
            FolderBrowserDialog folderBrowserDialog1 = new FolderBrowserDialog();
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                folderPath = folderBrowserDialog1.SelectedPath;
            }

            this.setPath.Location = new System.Drawing.Point(12, 3);
            this.setPath.Name = "setPath";
            this.setPath.Size = new System.Drawing.Size(80, 25);
            this.setPath.TabIndex = 4;
            this.setPath.Text = "path";
            this.setPath.UseVisualStyleBackColor = true;

            generalpath = folderPath;

            recognizer.Visible = true;
            fillTable();
        }

        private void fillTable()
        {
            string[] photos;
            if (Directory.Exists(generalpath))
            {
                photos = Directory.GetFiles(generalpath);


                pictures.Items.Clear();

                foreach (string path in photos)
                {
                    if (!(path.EndsWith(".jpg")))
                        continue;
                    ExifFile data = ExifFile.Read(path);
                    Settings.Default.Lastfile = path;
                    Settings.Default.Save();

                    ListViewItem lvitem = new ListViewItem(path);

                    string expTime = "", fnumber = "", isospeed = "", shutterSpeed = "", lat = "", longit = "";
                    double expTimeVal = 0, fnumberVal = 0, isospeedVal = 0, shutterSpeedVal = 0;
                    int xpixels = 0, ypixels = 0;

                    bool dateSetted = false;
                    foreach (ExifProperty item in data.Properties.Values)
                    {
                        if (item.Name == "ShutterSpeedValue")   // shutter speed value
                        {
                            shutterSpeed = item.ToString();
                            string[] stringSeparators = new string[] { "/" };
                            string[] result = expTime.Split(stringSeparators, StringSplitOptions.None);
                            if (Int32.Parse(result[1]) == 0)
                                shutterSpeedVal = 0;
                            else
                                shutterSpeedVal = Double.Parse(result[0]) / Double.Parse(result[1]);
                        }

                        if (item.Name == "ExposureTime")    // get exposure time
                        {
                            expTime = item.ToString();
                            string[] stringSeparators = new string[] { "/" };
                            string[] result = expTime.Split(stringSeparators, StringSplitOptions.None);
                            if (Int32.Parse(result[1]) == 0)
                                expTimeVal = 0;
                            else
                                expTimeVal = Double.Parse(result[0]) / Double.Parse(result[1]);
                        }

                        if (item.Name == "FNumber")     //get FNumber
                        {
                            fnumber = item.ToString();
                            string[] stringSeparators = new string[] { "/" };
                            string[] result = fnumber.Split(stringSeparators, StringSplitOptions.None);
                            if (Int32.Parse(result[1]) == 0)
                                fnumberVal = 0;
                            else
                                fnumberVal = Double.Parse(result[0]) / Double.Parse(result[1]);
                        }
                        if (item.Name == "ISOSpeedRatings") // get iso speed
                        {
                            isospeed = item.ToString();
                            isospeedVal = Double.Parse(isospeed);
                        }

                        if (item.Name == "DateTimeOriginal")
                        {
                            dateSetted = true;
                            // moment of time
                            string dateTime = item.ToString();
                            lvitem.SubItems.Add(dateTime);

                            // moment of day
                            DateTime dt = Convert.ToDateTime(dateTime);
                            string momOfDay = Variables.verifyMomentOfDay(dt);
                            lvitem.SubItems.Add(momOfDay);

                            lvitem.Tag = item;
                        }

                        if (item.Name == "GPSLatitude")
                            lat = item.ToString();
                        if (item.Name == "GPSLongitude")
                            longit = item.ToString();
                        if (item.Name == "PixelXDimension")
                            xpixels = Int32.Parse(item.ToString());
                        if (item.Name == "PixelYDimension")
                            ypixels = Int32.Parse(item.ToString());
                    }

                    double ev = Math.Log(fnumberVal * fnumberVal / expTimeVal, 2) + Math.Log(isospeedVal / 100, 2);
                    int evVal = (int)(Math.Round(ev, 0));

                    if (dateSetted == false)
                    { // not known date
                        lvitem.SubItems.Add("Unknown data time");
                        lvitem.SubItems.Add("Unknown moment");
                    }

                    string location = Variables.location(evVal, isospeedVal, shutterSpeedVal);
                    lvitem.SubItems.Add(location);  // indoor outdoor

                    // latitude
                    if (lat == "")
                        lvitem.SubItems.Add("Unknown");
                    else
                        lvitem.SubItems.Add(lat);
                    // longitude
                    if (longit == "")
                        lvitem.SubItems.Add("Unknown");
                    else
                        lvitem.SubItems.Add(longit);

                    lvitem.SubItems.Add(Variables.getQuality(xpixels, ypixels));
                    pictures.Items.Add(lvitem);
                }
            }
        }



        private void refillTable()
        {
            withRecogn = true;

            string[] photos;
            string expTime = "", fnumber = "", isospeed = "", shutterSpeed = "", lat = "", longit = "";
            double expTimeVal = 0, fnumberVal = 0, isospeedVal = 0, shutterSpeedVal = 0;
            int xpixels = 0, ypixels = 0;

            if (Directory.Exists(generalpath))
            {
                photos = Directory.GetFiles(generalpath);


                pictures.Items.Clear();

                foreach (string path in photos)
                {
                    if (!(path.EndsWith(".jpg")))
                        continue;
                    ExifFile data = ExifFile.Read(path);
                    Settings.Default.Lastfile = path;
                    Settings.Default.Save();

                    ListViewItem lvitem = new ListViewItem(path);

                    bool dateSetted = false;

                    foreach (ExifProperty item in data.Properties.Values)
                    {
                        if (item.Name == "ShutterSpeedValue")   // shutter speed value
                        {
                            shutterSpeed = item.ToString();
                            string[] stringSeparators = new string[] { "/" };
                            string[] result = expTime.Split(stringSeparators, StringSplitOptions.None);
                            if (Int32.Parse(result[1]) == 0)
                                shutterSpeedVal = 0;
                            else
                                shutterSpeedVal = Double.Parse(result[0]) / Double.Parse(result[1]);
                        }

                        if (item.Name == "ExposureTime")    // get exposure time
                        {
                            expTime = item.ToString();
                            string[] stringSeparators = new string[] { "/" };
                            string[] result = expTime.Split(stringSeparators, StringSplitOptions.None);
                            if (Int32.Parse(result[1]) == 0)
                                expTimeVal = 0;
                            else
                                expTimeVal = Double.Parse(result[0]) / Double.Parse(result[1]);
                        }

                        if (item.Name == "FNumber")     //get FNumber
                        {
                            fnumber = item.ToString();
                            string[] stringSeparators = new string[] { "/" };
                            string[] result = fnumber.Split(stringSeparators, StringSplitOptions.None);
                            if (Int32.Parse(result[1]) == 0)
                                fnumberVal = 0;
                            else
                                fnumberVal = Double.Parse(result[0]) / Double.Parse(result[1]);
                        }
                        if (item.Name == "ISOSpeedRatings") // get iso speed
                        {
                            isospeed = item.ToString();
                            isospeedVal = Double.Parse(isospeed);
                        }

                        if (item.Name == "DateTimeOriginal")
                        {
                            dateSetted = true; // has a date time
                            // moment of time
                            string dateTime = item.ToString();
                            lvitem.SubItems.Add(dateTime);

                            // moment of day
                            DateTime dt = Convert.ToDateTime(dateTime);
                            string momOfDay = Variables.verifyMomentOfDay(dt);
                            lvitem.SubItems.Add(momOfDay);

                            lvitem.Tag = item;
                        }

                        if (item.Name == "GPSLatitude")
                            lat = item.ToString();
                        if (item.Name == "GPSLongitude")
                            longit = item.ToString();
                        if (item.Name == "PixelXDimension")
                            xpixels = Int32.Parse(item.ToString());
                        if (item.Name == "PixelYDimension")
                            ypixels = Int32.Parse(item.ToString());
                    }

                    double ev = Math.Log(fnumberVal * fnumberVal / expTimeVal, 2) + Math.Log(isospeedVal / 100, 2);
                    int evVal = (int)(Math.Round(ev, 0));

                    if (dateSetted == false)
                    { // not known date
                        lvitem.SubItems.Add("Unknown data time");
                        lvitem.SubItems.Add("Unknown moment");
                    }
                    string location = Variables.location(evVal, isospeedVal, shutterSpeedVal);
                    lvitem.SubItems.Add(location);  // indoor outdoor
                    // latitude
                    if (lat == "")
                        lvitem.SubItems.Add("Unknown");
                    else
                        lvitem.SubItems.Add(lat);
                    // longitude
                    if (longit == "")
                        lvitem.SubItems.Add("Unknown");
                    else
                        lvitem.SubItems.Add(longit);

                    lvitem.SubItems.Add(Variables.getQuality(xpixels, ypixels));

                    int nrpersons = Recognition.nrFaces(path, Array.IndexOf(photos, path));
                    lvitem.SubItems.Add(nrpersons.ToString());

                    pictures.Items.Add(lvitem);

                    System.Threading.Thread.Sleep(100);
                }
            }

            // end working 
            this.pictureBox1.Visible = false;
            this.pictures.Enabled = true;
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            base.OnFormClosing(e);

            if (e.CloseReason == CloseReason.WindowsShutDown) return;

            // Confirm user wants to close
            switch (MessageBox.Show(this, "Are you sure you want to close?", "Close Automation Tool", MessageBoxButtons.YesNo))
            {
                case DialogResult.No:
                    e.Cancel = true;
                    break;
                default:
                    DirectoryInfo dir = new DirectoryInfo(Application.StartupPath + "/TrainedFaces/");
                    foreach(FileInfo fi in dir.GetFiles()){
                        fi.Delete();                        
                    }
                    break;
            }
        }

    }

}
