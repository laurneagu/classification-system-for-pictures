﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExifLibrary
{
    class Variables
    {
        public static string picturePath;
        public static int pictureIndex;

        public static string verifyMomentOfDay(DateTime date)
        {
            if (date.Hour >= 5 && date.Hour <= 11)
                return "morning";
            else
                if (date.Hour >= 12 && date.Hour <= 17)
                    return "noon";
                else
                    if (date.Hour >= 18 && date.Hour <= 21)
                        return "evening";
                    else
                        return "night";
        }

        public static string location(int ev,double isoSpeedVal,double shutterSpeedVal)
        {

            if (shutterSpeedVal != 0) // aproximez bine indoor/ outdoor
            {
                if (shutterSpeedVal <= 0.016) // outdoor
                {
                    return outdoor(ev);
                }
                else // indoor
                {
                    return indoor(ev);
                }
            }
            else    // use isoSpeedVal
            {
                if (isoSpeedVal <= 100)     //outdoor
                {
                    return outdoor(ev);
                }
                else
                    return indoor(ev);
            }
        }

        public static string getQuality(int pixX, int pixY)
        {
            if ((pixX <= 640 && pixY <= 480) || (pixY <= 640 && pixX <= 480))
                return "lower";
            else
                if ((pixX <= 1600 && pixY <= 1200) || (pixY <= 1600 && pixX <= 1200))
                    return "medium";
                else
                    return "high";
        } 


        public static string outdoor(int ev)
        {
            switch (ev)
            {
                case 2:
                    return "Distant views of lighted buildings";
                case 3:
                    return "Floodlit buildings / Christmas tree lights";
                case 4:
                    return "Floodlit buildings / Christmas tree lights";
                case 5:
                    return "Floodlit buildings / Christmas tree lights / Night vehicle traffic";
                case 6:
                    return "Street scenes";
                case 7:
                    return "Street scenes/ Fairs and amusement parks";
                case 8:
                    return "Street scenes/ Window displays";
                case 9:
                    return "Neon and other bright signs/ Night sports";
                case 10:
                    return "Neon and other bright signs/ Night sports";
                case 11:
                    return "Neon and other bright signs/ Night sports";
                case 12:
                    return "Areas in open shade";
                case 13:
                    return "Cloudy bright outside/ Sunset time";
                case 14:
                    return "Sunlight outside/ Sunset time / Moon";
                case 15:
                    return "Clear sky background/ Moon";
                case 16:
                    return "Clear sky background/ Moon";
                case 17:
                    return "Clear sky background/ Full moon";
                case 18:
                    return "Clear sky background/ Full moon";
                default:
                    return "Unknown"; // never here
            }
        }

        public static string indoor(int ev)
        {
            switch (ev)
            {

                case 4:
                    return "Indoor christmas tree lights";
                case 5:
                    return "Home interiors";
                case 6:
                    return "Home interiors";
                case 7:
                    return "Home interiors";
                case 8:
                    return "Home interiors/ Sports events, stage shows/ Galleries";
                case 9:
                    return "Home interiors/ Sports events, stage shows/ Galleries";
                case 10:
                    return "Home interiors/ Sports events, stage shows/ Galleries";
                case 11:
                    return "Home interiors/ Sports events, stage shows/ Galleries";
                case 12:
                    return "Home interiors/ Sports events, stage shows/ Galleries";
                default:
                    return "Unknown"; // never here
            }
        }
    }
}
