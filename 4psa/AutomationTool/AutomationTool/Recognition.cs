﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace ExifLibrary
{
    class Recognition
    {
        static Image<Gray, byte> gray = null;
        static HaarCascade face;
        static int t, ContTrain;
        public static List<Image<Gray, byte>> trainingImages = new List<Image<Gray, byte>>();
        public static List<Image<Bgr, byte>> recognizedImages = new List<Image<Bgr, byte>>();
        static List<string> labels = new List<string>();
        static List<string> NamePersons = new List<string>();
        static List<string> paths = new List<string>();
        static int personNr = 0;
        static int count = 0;

        static Image<Gray, byte> result, TrainedFace = null;

        public static int nrFaces(string path,int index)
        {
            face = new HaarCascade("haarcascade_frontalface_default.xml");
            path = path.Replace("\\", "/");
            //path = "@" + '"' + path + '"' ;

            int Place = path.LastIndexOf("/");
            string result2 = path.Remove(Place, "/".Length).Insert(Place, "\"$$$");
            path = result2;

            string[] stringSeparators = new string[] { "\"$$$" };
            string[] words = result2.Split(stringSeparators, StringSplitOptions.None);
            path = words[0];
            string currPath = Directory.GetFiles(path)[index];


            Image<Bgr, Byte> image = new Image<Bgr, byte>(currPath); //Read the files as an 8-bit Bgr image  
            gray = image.Convert<Gray, Byte>();

            //Face Detector
            MCvAvgComp[][] facesDetected = gray.DetectHaarCascade(face, 1.2, 10,Emgu.CV.CvEnum.HAAR_DETECTION_TYPE.DO_CANNY_PRUNING, new Size(20, 20));


            // save models

            //Action for each element detected
            foreach (MCvAvgComp f in facesDetected[0])
            {
                t = t + 1;
                result = image.Copy(f.rect).Convert<Gray, byte>().Resize(100, 100, Emgu.CV.CvEnum.INTER.CV_INTER_CUBIC);
                //draw the face detected in the 0th (gray) channel with blue color
                image.Draw(f.rect, new Bgr(Color.Red), 2);

                ContTrain = ContTrain + 1;      //add all models in array
                //resize face detected image for force to compare the same size with the 
                //test image with cubic interpolation type method
                TrainedFace = result;//.Resize(100, 100, Emgu.CV.CvEnum.INTER.CV_INTER_CUBIC);
                trainingImages.Add(TrainedFace);

                labels.Add("person" + trainingImages.ToArray().Length);//textBox1.Text);//textBox1.Text);
                personNr++;

                string fileP = Path.GetFileName(currPath);
                paths.Add(fileP);

                //Write the labels of triained faces in a file text for further load
                for (int j = count + 1; j < trainingImages.ToArray().Length + 1; j++)
                {
                    trainingImages.ToArray()[j - 1].Save(Application.StartupPath + "/TrainedFaces/" + fileP + "face" + (j - count) + ".bmp");
                }
            }

            t = 0;

            count = trainingImages.ToArray().Length; //+ 1;

            //Show the faces procesed and recognized

             image = image.Resize(320, 240, INTER.CV_INTER_AREA);

             recognizedImages.Add(image);

            // number of faces in frame
            return facesDetected[0].Length;
        }


        // similiarities between models

        /*
            private void similarities()
        {
            List<Image<Gray, Byte>> trImgAux = new List<Image<Gray, byte>>();
            Image<Gray, Byte>[] trVec = new Image<Gray, Byte>[trainingImages.ToArray().Length];
            trainingImages.ToArray().CopyTo(trVec,0);
            trImgAux = new List<Image<Gray, byte>>(trVec);

            List<string> labelsAux = new List<string>();
            string[] labelsVec = new string[trainingImages.ToArray().Length];
            labels.CopyTo(labelsVec);
            labelsAux = new List<string>(labelsVec);

            List<string> pathsAux = new List<string>();
            string[] pathsVec = new string[paths.ToArray().Length];
            paths.CopyTo(pathsVec);
            pathsAux = new List<string>(pathsVec);

            if (trainingImages.ToArray().Length != 0)
            {
                //TermCriteria for face recognition with numbers of trained images like maxIteration
                MCvTermCriteria termCrit = new MCvTermCriteria(ContTrain, 0.001);

                result = trImgAux.ToArray()[0];

                
                trImgAux.RemoveAt(0); // not itself
                labelsAux.RemoveAt(0);
                pathsAux.RemoveAt(0);
                

                //Eigen face recognizer
                EigenObjectRecognizer recognizer = new EigenObjectRecognizer(trImgAux.ToArray(), labelsAux.ToArray(),
                   3000, ref termCrit);
  
                name = recognizer.Recognize(result);

                MessageBox.Show(name + "!" + trImgAux.ToArray().Length.ToString() + " " + pathsAux.ToArray()[labelsAux.IndexOf(name)]);

                imageBox1.Image = trainingImages.ToArray()[labelsAux.IndexOf(name)];

                trImgAux.RemoveAt(labelsAux.IndexOf(name) );
                pathsAux.RemoveAt(labelsAux.IndexOf(name) );
                labelsAux.RemoveAt(labelsAux.IndexOf(name));
    
                
                //Draw the label for each face detected and recognized
                //image.Draw(name, ref font, new Point(f.rect.X - 2, f.rect.Y - 2), new Bgr(Color.LightGreen));

                while (name != "")
                {
                    recognizer = new EigenObjectRecognizer(trImgAux.ToArray(), labelsAux.ToArray(),
                  3000, ref termCrit);

                    name = recognizer.Recognize(result);
                    if (name != "")
                    {
                        MessageBox.Show(name + "!" + trImgAux.ToArray().Length.ToString() + " " + pathsAux.ToArray()[labelsAux.IndexOf(name) ]);
                        imageBox1.Image = trainingImages.ToArray()[labelsAux.IndexOf(name) + 1];

                        trImgAux.RemoveAt(labelsAux.IndexOf(name) );
                        pathsAux.RemoveAt(labelsAux.IndexOf(name) );
                        labelsAux.RemoveAt(labelsAux.IndexOf(name));
                    }

                }

            }
         */
    }
}
