﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ExifLibrary
{
    public partial class ViewModels : Form
    {
        public ViewModels()
        {
            InitializeComponent();
            var pc = new PictureBox();
            pc.Image = Recognition.trainingImages.ToArray()[0].ToBitmap();
            pc.Width = 320;
            pc.Height = 240;
            panel1.Controls.Add(pc);
        }


    }
}
